module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- | Een functie die de som van een lijst getallen berekent.
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 list = head list + ex1 (tail list)

-- | Een functie die alle elementen van een lijst met 1 ophoogt; bijvoorbeeld [1,2,3] -> [2,3,4].
ex2 :: [Int] -> [Int]
ex2 [x] = [x + 1]
ex2 (h : t) = ex2 [h] !! 0 : ex2 t

-- | Een functie die alle elementen van een lijst met -1 vermenigvuldigt; bijvoorbeeld [1,-2,3] -> [-1,2,-3].
ex3 :: [Int] -> [Int]
ex3 [x] = [-x]
ex3 (h : t) = ex3 [h] !! 0 : ex3 t

-- | Een functie die twee lijsten aan elkaar plakt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1,2,3,4,5,6].
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] right = right
ex4 (head_left : tail_left) right = head_left : ex4 tail_left right

-- | Een functie die twee lijsten paarsgewijs bij elkaar optelt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1+4, 2+5, 3+6] oftewel [5,7,9].
ex5 :: [Int] -> [Int] -> [Int]
ex5 l [] = l
ex5 (head_left : tail_left) (head_right:tail_right) = [head_left + head_right] ++ ex5 tail_left tail_right

-- | Een functie die twee lijsten paarsgewijs met elkaar vermenigvuldigt, dus bijvoorbeeld [1,2,3] en [4,5,6] combineert tot [1*4, 2*5, 3*6] oftewel [4,10,18].
ex6 :: [Int] -> [Int] -> [Int]
ex6 l [] = l
ex6 (head_left : tail_left) (head_right:tail_right) = [head_left * head_right] ++ ex6 tail_left tail_right

-- | Een functie die de functies uit opgave 1 en 6 combineert tot een functie die het inwendig product uitrekent. Bijvoorbeeld: `ex7 [1,2,3] [4,5,6]` -> 1*4 + 2*5 + 3*6 = 32.
ex7 :: [Int] -> [Int] -> Int
ex7 left right = ex1 (ex6 left right)
