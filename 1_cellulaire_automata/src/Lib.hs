{-|
    Module      : Lib
    Description : Tweede checkpoint voor V2DeP: cellulaire automata
    Copyright   : (c) Brian van der Bijl, 2020
    License     : BSD3
    Maintainer  : brian.vanderbijl@hu.nl

    In dit practicum gaan we aan de slag met 1D cellulaire automata [<https://mathworld.wolfram.com/Rule30.html>].
-}

module Lib where

import Data.Maybe (catMaybes) -- Niet gebruikt, maar deze kan van pas komen...
import Data.List (unfoldr)
import Data.Tuple (swap)

-- Om de state van een cellulair automaton bij te houden bouwen we eerst een set functies rond een `FocusList` type. Dit type representeert een 1-dimensionale lijst, met een
-- enkel element dat "in focus" is. Het is hierdoor mogelijk snel en makkelijk een enkele cel en de cellen eromheen te bereiken.

-- * FocusList

{- | The focussed list [0 1 2 ⟨3⟩ 4 5] is represented as @FocusList [3,4,5] [2,1,0]@. The first element (head) of the first list is focussed, and is easily and cheaply accessible.
 -   The items before the focus are placed in the backwards list in reverse order, so that we can easily move the focus by removing the focus-element from one list and prepending
 -   it to the other.
-}
data FocusList a = FocusList { forward :: [a]
                             , backward :: [a]
                             }
  deriving Show

-- De instance-declaraties mag je voor nu negeren.
instance Functor FocusList where
  fmap = mapFocusList

-- Enkele voorbeelden om je functies mee te testen:
intVoorbeeld :: FocusList Int
intVoorbeeld = FocusList [3,4,5] [2,1,0]

stringVoorbeeld :: FocusList String
stringVoorbeeld = FocusList ["3","4","5"] ["2","1","0"]

-- | Converts a focus-list to a list. note the list doesn't contain any focus information anymore. It does this by combining the forward and the reversed backward.
-- toList intVoorbeeld ~> [0,1,2,3,4,5]
toList :: FocusList a -> [a]
toList (FocusList f b) = reverse b ++ f

-- | Converts a normal list to a focus-list, the focus will be set on the first element in the list. It does this by creating a focus-list with the given list as forward and an empty backward list
fromList :: [a] -> FocusList a
fromList l = FocusList l []

-- | Move the focus one to the left
goLeft :: FocusList a -> FocusList a
goLeft (FocusList fw (f:bw)) = FocusList (f:fw) bw

-- | Move the focus one to the right
goRight :: FocusList a -> FocusList a
goRight (FocusList (f:fw) b) = FocusList fw (f:b)

-- | Moves the focus to the left most element
leftMost :: FocusList a -> FocusList a
leftMost (FocusList f b) = FocusList (reverse b ++ f) []

-- | Moves the focus to the right most element
rightMost :: FocusList a -> FocusList a
rightMost fl = FocusList [last list] $ reverse $ init list
  where list = toList fl

-- De functies goLeft en goRight gaan er impliciet vanuit dat er links respectievelijk rechts een cell gedefinieerd is. De aanroep `goLeft $ fromList [1,2,3]` zal echter crashen
-- omdat er in een lege lijst gezocht wordt: er is niets links. Dit is voor onze toepassing niet handig, omdat we bijvoorbeeld ook de context links van het eerste vakje nodig
-- hebben om de nieuwe waarde van dat vakje te bepalen (en dito voor het laatste vakje rechts).

-- [⟨░⟩, ▓, ▓, ▓, ▓, ░]  ⤚goLeft→ [⟨░⟩, ░, ▓, ▓, ▓, ▓, ░]

-- | Moves the focus one to the left, but if there isn't any element there, it will complement with mempty
totalLeft :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalLeft fl = if length (backward fl) == 0
  then FocusList (mempty : (toList fl)) []
  else goLeft fl

-- | Moves the focus one to the right, but if there isn't any element there, it will complement with mempty
totalRight :: (Eq a, Monoid a) => FocusList a -> FocusList a
totalRight fl = if length (forward fl) == 1
  then FocusList [mempty] $ head (forward fl) : backward fl 
  else goRight fl

-- | Maps a function on a focuslist. It does this by just applying the function to the forward and the backward
mapFocusList :: (a -> b) -> FocusList a -> FocusList b
mapFocusList f (FocusList for bac) = FocusList (map f for) $ map f bac

-- De functie zipFocusList zorgt ervoor dat ieder paar elementen uit de FocusLists als volgt met elkaar gecombineerd wordt:
-- [1, 2, ⟨3⟩,  4, 5]
-- [  -1, ⟨1⟩, -1, 1, -1]
--------------------------- (*)
-- [  -2, ⟨3⟩, -4, 5    ]

-- Oftewel: de megegeven functie wordt aangeroepen op de twee focus-elementen, met als resultaat het nieuwe focus-element. Daarnaast wordt de functie paarsgewijs naar
-- links/rechts doorgevoerd, waarbij gestopt wordt zodra een van beide uiteinden leeg is. Dit laatste is net als bij de gewone zipWith, die je hier ook voor mag gebruiken.

-- | Zips two focuslists with a given function. It does this by just zipping the two forwards and the two backwards with the function
zipFocusListWith :: (a -> b -> c) -> FocusList a -> FocusList b -> FocusList c
-- didn't know zipWith handels lists of two different lengths just fine (thought it would give a error) until after I made this, but now it would be a shame to throw the code away
zipFocusListWith f (FocusList for1 bac1) (FocusList for2 bac2) = 
  let
    for_length = min (length for1) (length for2)
    bac_length = min (length bac1) (length bac2)
  in FocusList 
  (zipWith f (take for_length for1) (take for_length for2))
  (zipWith f (take bac_length bac1) (take bac_length bac2))


-- Het folden van een FocusList vergt de meeste toelichting: waar we met een normale lijst met een left fold en een right fold te maken hebben, moeten we hier vanuit de focus werken.
-- Vanuit de focus worden de elementen van rechts steeds gecombineerd tot een nieuw element, vanuit het element voor de focus gebeurt hetzelfde vanuit links. De twee resultaten van
-- beide sublijsten (begin tot aan focus, focus tot en met eind) worden vervolgens nog een keer met de meegegeven functie gecombineerd. Hieronder een paar voorbeelden:

-- foldFocusList (*) [0, 1, 2, ⟨3⟩, 4, 5] = (0 * (1 * 2)) * ((3 * 4) * 5)

-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = (0 - (1 - 2)) - ((3 - 4) - 5)
-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = (0 - (-1)) - ((-1) - 5)
-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = 1 - (-6)
-- foldFocusList (-) [0, 1, 2, ⟨3⟩, 4, 5] = 7

-- Je kunt `testFold` gebruiken om je functie te testen. Denk eraan dat de backwards lijst achterstevoren staat, en waarschijnlijk omgekeerd moet worden.

-- | Folding a focuslist with a given function. It does this by folding the forward and the backward and then apply the function to combine those two
foldFocusList :: (a -> a -> a) -> FocusList a -> a
foldFocusList f (FocusList for bac) = f (foldr f (last rbac) (init rbac)) (foldl f (head for) (tail for))
  where rbac = reverse bac


-- | Test function for the behaviour of foldFocusList.
testFold :: Bool
testFold = and [ foldFocusList (+) intVoorbeeld     == 15
               , foldFocusList (-) intVoorbeeld     == 7
               , foldFocusList (++) stringVoorbeeld == "012345"
               ]

-- * Cells and Automata

-- Nu we een redelijk complete FocusList hebben kunnen we gaan kijken naar daadwerkelijke celulaire automata, te beginnen met de Cell.

-- | A cell can be either on or off, dead or alive. What basic type could we have used instead? Why would we choose to roll our own equivalent datatype?
data Cell = Alive | Dead deriving (Show, Eq)

-- De instance-declaraties mag je voor nu negeren.
instance Semigroup Cell where
  Dead <> x = x
  Alive <> x = Alive

instance Monoid Cell where
  mempty = Dead

-- | The state of our cellular automaton is represented as a FocusList of Cells.
type Automaton = FocusList Cell

-- | Start state, per default, is a single live cell.
start :: Automaton
start = FocusList [Alive] []

-- | Alternative start state with 5 alive cells, for shrinking rules.
fiveAlive :: Automaton
fiveAlive = fromList $ replicate 5 Alive

-- | A rule [<https://mathworld.wolfram.com/Rule30.html>] is a mapping from each possible combination of three adjacent cells to the associated "next state".
type Context = [Cell]
type Rule = Context -> Cell

-- * Rule Iteration

-- | Gives the first element of a list, when there is none it will give the given default value
safeHead :: a        -- ^ Default value
         -> [a]      -- ^ Source list
         -> a
safeHead d s = if length s == 0 then d else head s  -- probably would have been prettier with pattern matching

-- `takeAtLeast 3 "0" ["1","2","3","4","5"]` dan werkt de functie hetzelfde als `take` en worden de eerste `n` (hier 3) elementen teruggegeven.
-- Als dat niet zo is dan worden zoveel mogelijk elementen teruggegeven, en wordt de lijst daarna tot de gevraagde lengte aangevuld met een
-- meegegeven default-waarde: `takeAtLeast 3 "0" ["1"] ~> ["1", "0", "0"]`.
-- | Gives the first n elements of the given list, when there aren't enough it will supplement with the given default value
takeAtLeast :: Int   -- ^ Number of items to take
            -> a     -- ^ Default value added to the right as padding
            -> [a]   -- ^ Source list
            -> [a]
takeAtLeast n d s = take n s ++ replicate (n - length s) d

-- | Gives the context of a given automaton, aka it gives the selected element and the two serounding it
context :: Automaton -> Context
context (FocusList f b) = takeAtLeast 1 Dead b ++ (takeAtLeast 2 Dead f)

-- | Returns a copy of the gives automaton but with a dead cell added on each side
expand :: Automaton -> Automaton
expand (FocusList f b) = FocusList (f ++ [Dead]) (b ++ [Dead])

-- | A sequence of Automaton-states over time is called a TimeSeries.
type TimeSeries = [Automaton]

-- | Iterate a given rule @n@ times, given a start state. The result will be a sequence of states from start to @n@.
iterateRule :: Rule          -- ^ The rule to apply
            -> Int           -- ^ How many times to apply the rule
            -> Automaton     -- ^ The initial state
            -> TimeSeries
iterateRule r 0 s = [s] -- when all the iterations are done, return the end automaton
iterateRule r n s = s : iterateRule r (pred n) (fromList $ applyRule $ leftMost $ expand s) -- apply recursion with the same rule, one less iteration to go, and process this Automaton: (expand the Automaton, select the first cell, apply the rule to the Automaton)
  where applyRule :: Automaton -> Context -- the function that iterates the rule over each cell
        applyRule (FocusList [] bw) = []  -- when it is at the end of the Automaton, return an empty list
        applyRule z = r (context z) : applyRule (goRight z) -- apply the rule to the context around the selected and prepend that to the other results from the right

-- | Convert a time-series of Automaton-states to a printable string.
showPyramid :: TimeSeries -> String
showPyramid zs = unlines $ zipWith showFocusList zs $ reverse [0..div (pred w) 2]
  where w = length $ toList $ last zs :: Int
        showFocusList :: Automaton -> Int -> String
        showFocusList z p = replicate p ' ' <> concatMap showCell (toList z)
        showCell :: Cell -> String
        showCell Dead  = "░"
        showCell Alive = "▓"

-- Hard coded rule30 by using pattern matching
rule30 :: Rule
rule30 [Dead, Dead, Dead] = Dead
rule30 [Dead, Dead, Alive] = Alive
rule30 [Dead, Alive, Dead] = Alive
rule30 [Dead, Alive, Alive] = Alive
rule30 [Alive, Dead, Dead] = Alive
rule30 [Alive, Dead, Alive] = Dead
rule30 [Alive, Alive, Dead] = Dead
rule30 [Alive, Alive, Alive] = Dead

-- Je kan je rule-30 functie in GHCi testen met het volgende commando:
-- putStrLn . showPyramid . iterateRule rule30 15 $ start

-- De verwachte uitvoer is dan:
{-             ▓
              ▓▓▓
             ▓▓░░[0 1 2 ⟨3⟩ 4 5]▓
            ▓▓░▓▓▓▓
           ▓▓░░▓░░░▓
          ▓▓░▓▓▓▓░▓▓▓
         ▓▓░░▓░░░░▓░░▓
        ▓▓░▓▓▓▓░░▓▓▓▓▓▓
       ▓▓░░▓░░░▓▓▓░░░░░▓
      ▓▓░▓▓▓▓░▓▓░░▓░░░▓▓▓
     ▓▓░░▓░░░░▓░▓▓▓▓░▓▓░░▓
    ▓▓░▓▓▓▓░░▓▓░▓░░░░▓░▓▓▓▓
   ▓▓░░▓░░░▓▓▓░░▓▓░░▓▓░▓░░░▓
  ▓▓░▓▓▓▓░▓▓░░▓▓▓░▓▓▓░░▓▓░▓▓▓
 ▓▓░░▓░░░░▓░▓▓▓░░░▓░░▓▓▓░░▓░░▓
▓▓░▓▓▓▓░░▓▓░▓░░▓░▓▓▓▓▓░░▓▓▓▓▓▓▓ -}

-- * Rule Generation

-- Er bestaan 256 regels, die we niet allemaal met de hand gaan uitprogrammeren op bovenstaande manier. Zoals op de genoemde pagina te zien is heeft het nummer te maken met binaire
-- codering. De toestand van een cel hangt af van de toestand van 3 cellen in de vorige ronde: de cel zelf en diens beide buren (de context). Er zijn 8 mogelijke combinaties
-- van 3 van dit soort cellen. Afhankelijke van het nummer dat een regel heeft mapt iedere combinatie naar een levende of dode cel.

-- | All the eight binary possibilities for 3 bits given in Alive or Dead
inputs :: [Context]
inputs = [[x,y,z] | x<-bin, y<-bin, z<-bin] where bin=[Alive, Dead]

-- | If the given predicate applies to the given value, return Just the given value; in all other cases, return Nothing.
guard :: (a -> Bool) -> a -> Maybe a
guard p x | p x = Just x
          | otherwise = Nothing

-- TODO Deze functie converteert een Int-getal naar een binaire representatie [Bool]. Zoek de definitie van `unfoldr` op met Hoogle en `guard` in Utility.hs; `toEnum` converteert
-- een Int naar een ander type, in dit geval 0->False en 1->True voor Bool. Met deze kennis, probeer te achterhalen hoe de binary-functie werkt en documenteer dit met Haddock.
-- | Converts a integer value to the corrosponding binary value giving in True and False. Values higher than 255 will overflow, so binary 256 gives the same as binary 0
binary :: Int -> [Bool]
binary = map toEnum . reverse . take 8 . (++ repeat 0)  -- honestly this goes over my head, but I would really like to learn how it works
       . unfoldr (guard (/= (0,0)) . swap . flip divMod 2)

-- | Filters a list of elements on the corrosponding booleans on the same index. It does this by making every element that belongs with a False to Nothing and then filters those out with catMaybes
mask :: [Bool] -> [a] -> [a]
mask bl el = catMaybes $ zipWith (\b e -> if b then Just e else Nothing) bl el

-- | Returns a usable rule based on the given rule number. Masking binary n and inputs results in a list of patterns that need to return Alive, so then it is just checking if the given input is in that list, if so: Alive if not: Dead
rule :: Int -> Rule
rule n input
  | elem input (mask (binary n) inputs) = Alive
  | otherwise = Dead

{- Je kan je rule-functie in GHCi testen met variaties op het volgende commando:

   putStrLn . showPyramid . iterateRule (rule 18) 15 $ start

                  ▓
                 ▓░▓
                ▓░░░▓
               ▓░▓░▓░▓
              ▓░░░░░░░▓
             ▓░▓░░░░░▓░▓
            ▓░░░▓░░░▓░░░▓
           ▓░▓░▓░▓░▓░▓░▓░▓
          ▓░░░░░░░░░░░░░░░▓
         ▓░▓░░░░░░░░░░░░░▓░▓
        ▓░░░▓░░░░░░░░░░░▓░░░▓
       ▓░▓░▓░▓░░░░░░░░░▓░▓░▓░▓
      ▓░░░░░░░▓░░░░░░░▓░░░░░░░▓
     ▓░▓░░░░░▓░▓░░░░░▓░▓░░░░░▓░▓
    ▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓░░░▓
   ▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓░▓

   putStrLn . showPyramid . iterateRule (rule 128) 10 $ fiveAlive

               ▓▓▓▓▓
              ░░▓▓▓░░
             ░░░░▓░░░░
            ░░░░░░░░░░░
           ░░░░░░░░░░░░░
          ░░░░░░░░░░░░░░░
         ░░░░░░░░░░░░░░░░░
        ░░░░░░░░░░░░░░░░░░░
       ░░░░░░░░░░░░░░░░░░░░░
      ░░░░░░░░░░░░░░░░░░░░░░░
     ░░░░░░░░░░░░░░░░░░░░░░░░░

   Als het goed is zal `stack run` nu ook werken met de voorgeschreven main functie; experimenteer met verschillende parameters en zie of dit werkt.
-}
